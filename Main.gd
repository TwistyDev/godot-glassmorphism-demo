extends Node2D

onready var profile_pic = $UI/login/PointBlur/point/icon
onready var name_field = $UI/login/LineEdit
onready var password_field = $UI/login/LineEdit2
onready var timer = $UI/login/Timer

var loading = false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var anim_player = $AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	anim_player.play("Floating")


func _on_LineEdit_text_changed(new_text):
	profile_pic.texture = load("res://assets/loading.png")
	anim_player.stop(true)
	anim_player.play("loading")
	timer.start()
	
func update_picture():
	print('j')
	anim_player.stop(true)
	profile_pic.modulate.a=1.0
	if name_field.text == "godette@godot.com":
		profile_pic.texture = load("res://assets/profilepicture.png")
	else:
		profile_pic.texture = load("res://assets/user.png")
	timer.stop()

func _on_LoginButton_pressed():
#	anim_player.play("Transition")
	if name_field.text == "godette@godot.com" and password_field.text == "password":
		print('Access granted !')
		$Tween.interpolate_property($UI/login,"rect_position:x",$UI/login.rect_position.x,$UI/login.rect_position.x-1920,1.0,Tween.TRANS_EXPO,Tween.EASE_IN)
		$Tween.interpolate_property($UI/login/ProgressBar,"value",0,60,2.0,Tween.TRANS_CUBIC,Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween,"tween_all_completed")
		$Tween.interpolate_property($UI/login/ProgressBar,"value",60,100,1.0,Tween.TRANS_EXPO,Tween.EASE_IN_OUT)
		$Tween.start()
		yield($Tween,"tween_all_completed")
		$UI/login/Troll.play()
		$Tween.interpolate_property($UI/login/Troll,"rect_position:y",1090,0,1.0,Tween.TRANS_EXPO,Tween.EASE_OUT)
		$Tween.start()
	else:
		if not anim_player.is_playing():
			anim_player.play('login_fail')
		name_field.text = ""
		password_field.text = ""
